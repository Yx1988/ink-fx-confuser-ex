﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InkFx.ConfuserEx.Utils;
using Microsoft.Win32;

namespace InkFx.ConfuserEx
{
    class Program
    {
        static int Main(string[] args)
        {
            

            if (args != null && args.Length >= 1)
            {
                if (args[0].Trim('-', '/', '\\').Trim().ToLower() == "r")
                {
                    InitSysMenu();
                    Console.ReadKey();
                    return 0;
                }
                else if (args[0].Trim('-', '/', '\\').Trim().ToLower() == "u")
                {
                    UnRegSysMenu();
                    Console.ReadKey();
                    return 0;
                }
                else if (args[0].Trim('-', '/', '\\').Trim().ToLower() == "d")
                {
                    if (args.Length <= 0)
                    {
                        Tools.LogWarn("缺少程序集路径! \r\n请使用 -d \"E:\a.dll\" 混淆指定路径的程序集");
                        return 1;
                    }
                    //File.WriteAllText(@"D:\Test.txt", string.Join("\r\n\r\n", args));
                    List<string> listFile = args[1].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().Select(x => x.Trim('"', '\'').Trim()).ToList();
                    foreach (string temp in listFile)
                    {
                        string file = new FileInfo(temp).FullName;
                        if (!file.EndsWith(".dll", StringComparison.CurrentCultureIgnoreCase) && !file.EndsWith(".exe", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Tools.LogWarn(@"跳过混淆: {0}", file);
                            continue;
                        }

                        bool result = ConfuserExHelper.ConfuseAssembly(file);
                        if (result) { Tools.LogOK(string.Format(@"混淆成功: {0}", file)); }
                        else { Tools.LogError(string.Format(@"混淆失败: {0}", file)); }
                        Console.WriteLine("按任意键结束...");
                        Console.ReadKey();
                    }
                    return 0;
                }
            }


            Console.WriteLine("请输入正确的参数: \r\n -r 注册系统右键菜单 \r\n -u 卸载系统右键菜单 \r\n -d \"E:\a.dll\" 混淆指定路径的程序集");
            Console.ReadKey();
            return 0;
        }





        private static void InitSysMenu()
        {
            try
            {
                UnRegSysMenu(@"混淆程序集");
                RegSysMenu(@"混淆程序集", Application.ExecutablePath, "-d");
                Console.WriteLine(@"注册系统右键菜单【混淆程序集】完成!");
            }
            catch (Exception) { }
        }
        private static void UnRegSysMenu()
        {
            try
            {
                UnRegSysMenu(@"混淆程序集");
                Console.WriteLine(@"卸载系统右键菜单【混淆程序集】完成!");
            }
            catch (Exception) { }
        }

        #region  注册右键菜单

        private static bool RegSysMenu(string menuName, string appPath, string appArg)
        {
            //注册到所有文件
            {
                RegistryKey shell = Registry.ClassesRoot.OpenSubKey(@"*\shell", true);
                RegistryKey custom = shell.CreateSubKey(menuName);
                RegistryKey cmd = custom.CreateSubKey("command");
                cmd.SetValue(string.Empty, appPath + " " + appArg + " %1");  //%1 是传入打开的文件路径
                cmd.Close();
                custom.Close();
                shell.Close();
            }

            ////注册到所有目录
            //{
            //    RegistryKey shell = Registry.ClassesRoot.OpenSubKey(@"directory\shell", true);
            //    RegistryKey custom = shell.CreateSubKey(menuName);
            //    RegistryKey cmd = custom.CreateSubKey("command");
            //    cmd.SetValue("", Application.ExecutablePath + " %1"); //%1 是传入打开的文件路径
            //    cmd.Close();
            //    custom.Close();
            //    shell.Close();
            //}

            return true;
        }
        private static bool UnRegSysMenu(string menuName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(menuName))
                    throw new Exception("需要删除的 系统菜单 名称无效!");


                //注册到所有文件
                {
                    RegistryKey shell = Registry.ClassesRoot.OpenSubKey(@"*\shell", true);
                    shell.DeleteSubKeyTree(menuName);
                    shell.Close();
                }

                ////注册到所有目录
                //{
                //    RegistryKey shell = Registry.ClassesRoot.OpenSubKey(@"directory\shell", true);
                //    shell.DeleteSubKeyTree(menuName);
                //    shell.Close();
                //}

                return true;
            }
            catch (Exception) { return false; }
        }

        #endregion

    }
}
