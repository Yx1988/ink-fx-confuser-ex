# InkFx.ConfuserEx

#### 介绍
一键混淆 .Net 程序集。
基于已开源的 第三方程序 ConfuserEx。为其扩展 右键菜单，自动生成混淆方案，真正做到 一键混淆。

> PS.记得修改 exe.config 文件，指向 ConfuserEx 路径。


#### 安装教程 使用说明

![注册右键菜单](https://images.gitee.com/uploads/images/2021/0407/113735_8666ff2f_4831650.png "360截图1640022596121110.png")

![选中dll混淆](https://images.gitee.com/uploads/images/2021/0407/113756_a9aee795_4831650.png "360截图164912148410197.png")

![代码混淆过程](https://images.gitee.com/uploads/images/2021/0407/113818_88204ceb_4831650.png "360截图1653070989104112.png")

![代码混淆结果](https://images.gitee.com/uploads/images/2021/0407/113851_07aeb42b_4831650.png "360截图16581114969995.png")


#### 资源引用

本程序只是在 已开源的第三方程序 ConfuserEx 基础上的扩展。
ConfuserEx 原始地址： https://github.com/yck1509/ConfuserEx 


